/* 
    ==== String Problem #3 ====
    Given a string in the format of "10/1/2021", print the month in which the date is present in.
*/

function string3(str) {

    const date = str.split('/');
    if(date.length !== 3) {
        return '';
    }

    const month = Number(date[1]);
    if(isNaN(month) || month <= 0 || month > 12) {
        return '';
    }

    return getMonth(month);
}

function getMonth(month) {
    const monthObj = { 1: 'January', 2: 'Febuary', 3: 'March', 4: 'April', 5: 'May', 6: 'June',
        7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'
    }

    return monthObj[month];
}

// const result = string3('10/11/2021');
// console.log(result);

module.exports = string3;
