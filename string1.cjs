/*  
    ==== String Problem #1 ====

    There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. 
    Write a function to convert the given strings into their equivalent numeric format without 
    any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the 
    string so if the number is invalid, return 0. 
*/

function string1(str) {

    let ans = '', delimeterPos = 1;

    if(str.charAt(0) === '-' || str.charAt(0) === '+') {
        str = str.slice(1);
        ans += '-';
    }

    if(str.charAt(0) === '$') {
        str = str.slice(1);
    }

    const arr = str.split('.');
    if(arr.length > 2) {
        return 0;
    }

    for(let index = 0; index < arr[0].length; index++) {
        if(arr[0][index] === ',' && arr[0].length >= 4) {
            if(index % 4 === 1) {
                continue;
            } else {
                return 0;
            }
        } else if(arr[0][index] >= '0' && arr[0][index] <= '9') {
            ans += arr[0][index];
        } else {
            return 0;
        }
    }

    if(arr.length > 1) {
        ans += '.';
    }

    for(let index = 0; index < arr[1].length; index++) {
        const ch = arr[1][index];
        if(arr[1][index] >= '0' && arr[1][index] <= '9') {
            ans += ch;
        } else {
            return 0;
        }
    }
    // console.log(arr);
    return Number(ans)
}

// const result = string1('$1,002.22');
// console.log(result)

module.exports = string1;
