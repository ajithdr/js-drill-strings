/*
    ==== String Problem #4 ====
    Given an object in the following format, return the full name in title case.
    {"first_name": "JoHN", "last_name": "SMith"}
    {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}
*/

function string4(obj) {

    let fullName = '';

    for(let name in obj) {
        fullName += obj[name][0].toUpperCase() + obj[name].slice(1).toLowerCase() + ' ';
    }

    const length = fullName.length;
    return fullName.slice(0, length-1);
}

// const result = string4({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"});
// console.log(result);

module.exports = string4;
