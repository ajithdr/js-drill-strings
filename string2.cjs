/*
    ==== String Problem #2 ====
    Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 
    and return it in an array in numeric values. [111, 139, 161, 143].
    Support IPV4 addresses only. If there are other characters detected, return an empty array.
*/

function string2(str) {

    const ipAddr = str.split('.');

    if(ipAddr.length !== 4) {
        return [];
    }

    for(let index = 0; index < ipAddr.length; index++) {
        const val = Number(ipAddr[index]);
        if(isNaN(val)) {
            return [];
        } else {
            if(val >= 0 && val <= 255) {
                ipAddr[index] = val;
            } else {
                return [];
            }
        }
    }

    return ipAddr;
}

// const result = string2('111.139.161.143');
// console.log(result);

module.exports = string2;