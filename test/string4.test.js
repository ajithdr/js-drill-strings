const string4 = require('../string4.cjs');

test('String4', () => {
    expect(string4({"first_name": "JoHN", "last_name": "SMith"})).toBe('John Smith')
});