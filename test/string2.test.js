const string2 = require('../string2.cjs');

test('String2', () => {
    expect(string2('111.139.161.143')).toStrictEqual([111, 139, 161, 143])
});